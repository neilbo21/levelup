import { Page } from './app.po';
import { browser } from 'protractor';

describe('App', () => {
  let page: Page;

  beforeEach(() => {
    page = new Page();
  });

  describe('default screen', () => {
    beforeEach(() => {
      page.navigateTo('');
    });

    it('should have a title saying Home', () => {
      let titleElement = page.getTitle();
      expect(titleElement).toContain('Home');
      // browser.driver.sleep(3000);
    });
  })
});
