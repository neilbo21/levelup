import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, Platform, NavController } from 'ionic-angular/index';
import { PlatformMock, StatusBarMock, SplashScreenMock } from '../../../test-config/mocks.ionic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AboutPage } from '../about/about';

describe('Page: About Page', () => {

    let de: DebugElement;
    let comp: AboutPage;
    let fixture: ComponentFixture<AboutPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AboutPage],
            imports: [
                IonicModule.forRoot(AboutPage)
            ],
            providers: [
                NavController,
                { provide: Platform, useClass: PlatformMock },
                { provide: StatusBar, useClass: StatusBarMock },
                { provide: SplashScreen, useClass: SplashScreenMock },
            ],

        }).compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AboutPage);
        comp = fixture.componentInstance;
    });

    it('is created', () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

    it('should have expected <ion-title> text', () => {
        de = fixture.debugElement.query(By.css('ion-title'));
        const _title = de.nativeElement;
        
        fixture.detectChanges();
        
        expect(_title.innerText).toMatch(/About/i,
            '<ion-title> should say something about "About"');
    });
   
    it('initialises with a title of About', () => {
        expect(comp['title']).toEqual('About');
    });

    it('can set the title to a supplied value', () => {
        de = fixture.debugElement.query(By.css('ion-title'));
        const _title = de.nativeElement;
        
        comp.setTitle('Some other title');
        fixture.detectChanges();

        expect(comp['title']).toEqual('Some other title');
        expect(_title.textContent).toContain('Some other title');
    });
});