import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  title:string = 'About';
  constructor(
      public navCtrl: NavController
    ) {
  }

  setTitle(title) {
    this.title = title
  }

}
