import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MyApp } from '../../app/app.component';
import { TabsPage } from './tabs';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { Platform } from 'ionic-angular/platform/platform';
import { PlatformMock, StatusBarMock, SplashScreenMock, NavMock } from '../../../test-config/mocks.ionic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

let de: DebugElement;
let comp: TabsPage;
let fixture: ComponentFixture<TabsPage>;

describe('Page: Tabs Page', () => {

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [TabsPage],
            imports: [
                IonicModule.forRoot(TabsPage)
            ],
            providers: [

                { provide: NavController, useClass: NavMock },
                { provide: Platform, useClass: PlatformMock },
                { provide: StatusBar, useClass: StatusBarMock },
                { provide: SplashScreen, useClass: SplashScreenMock },
            ],

        }).compileComponents();

    }));

    beforeEach(() => {

        fixture = TestBed.createComponent(TabsPage);
        comp = fixture.componentInstance;

    });

    it('is created', () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });
    /*
    Skipped tests below because of reasons:
    https://stackoverflow.com/questions/44137146/ionic-3-with-tabs-error-during-cleanup-of-component
    https://stackoverflow.com/questions/43278192/setting-up-tdd-in-ionic2/45775194#45775194
    */ 
    xit('sets tab1Root to HomePage', () => {
        expect(comp.tab1Root).toBe(HomePage);
        expect(comp.tab1Root.name).toBe('HomePage');
    });

    xit('sets tab2Root to AboutPage', () => {
        expect(comp.tab2Root).toBe(AboutPage);
        expect(comp.tab2Root.name).toBe('AboutPage');

    });

    xit('sets tab3Root to ContactPage', () => {
        expect(comp.tab3Root).toBe(ContactPage);
        expect(comp.tab3Root.name).toBe('ContactPage');
    });
});