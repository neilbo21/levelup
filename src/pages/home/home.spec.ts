import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, Platform, NavController } from 'ionic-angular/index';
import { PlatformMock, StatusBarMock, SplashScreenMock } from '../../../test-config/mocks.ionic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../home/home';

describe('Page: Home Page', () => {

    let de: DebugElement;
    let comp: HomePage;
    let fixture: ComponentFixture<HomePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomePage],
            imports: [
                IonicModule.forRoot(HomePage)
            ],
            providers: [
                NavController,
                { provide: Platform, useClass: PlatformMock },
                { provide: StatusBar, useClass: StatusBarMock },
                { provide: SplashScreen, useClass: SplashScreenMock },
            ],

        }).compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomePage);
        comp = fixture.componentInstance;
    });

    it('is created', () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

    it('should have expected <h2> text', () => {
        de = fixture.debugElement.query(By.css('h2'));
        const h2 = de.nativeElement;
        
        fixture.detectChanges();
        
        expect(h2.innerText).toMatch(/ionic/i,
            '<h2> should say something Ionic "Ionic"');
    });
   
    it('initialises with a title of Home', () => {
        expect(comp['title']).toEqual('Home');
    });

    it('can set the title to a supplied value', () => {
        de = fixture.debugElement.query(By.css('ion-title'));
        const _title = de.nativeElement;
        
        comp.setTitle('Some other title');
        fixture.detectChanges();

        expect(comp['title']).toEqual('Some other title');
        expect(_title.textContent).toContain('Some other title');
    });
});