import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  title:string = 'Contact';

  constructor(public navCtrl: NavController) {

  }

  setTitle(title) {
    this.title = title;
  }

}
