import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { IonicModule, Platform, NavController } from 'ionic-angular/index';
import { PlatformMock, StatusBarMock, SplashScreenMock } from '../../../test-config/mocks.ionic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactPage } from '../contact/contact';

describe('Page: Contact Page', () => {

    let de: DebugElement;
    let comp: ContactPage;
    let fixture: ComponentFixture<ContactPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ContactPage],
            imports: [
                IonicModule.forRoot(ContactPage)
            ],
            providers: [
                NavController,
                { provide: Platform, useClass: PlatformMock },
                { provide: StatusBar, useClass: StatusBarMock },
                { provide: SplashScreen, useClass: SplashScreenMock },
            ],

        }).compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContactPage);
        comp = fixture.componentInstance;
    });

    it('is created', () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

    it('should have expected <ion-title> text', () => {
        de = fixture.debugElement.query(By.css('ion-title'));
        const _title = de.nativeElement;
        
        fixture.detectChanges();
        
        expect(_title.innerText).toMatch(/contact/i,
            '<ion-title> should say something about "Contact"');
    });
   
    it('initialises with a title of Contact', () => {
        expect(comp['title']).toEqual('Contact');
    });

    it('can set the title to a supplied value', () => {
        de = fixture.debugElement.query(By.css('ion-title'));
        const _title = de.nativeElement;
        
        comp.setTitle('Some other title');
        fixture.detectChanges();

        expect(comp['title']).toEqual('Some other title');
        expect(_title.textContent).toContain('Some other title');
    });
});