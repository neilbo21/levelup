#Ionic Level Up

This is a simple Ionic App that is meant to showcase:
- [x] TDD approach
- [x] Setup Unit Tests
- [x] Running Unit Tests in BitBucket Pipelines
- [x] Added Test Coverage with Istanbul Reporter
- [ ] Lazy Loading
- [ ] Auth with Firebase
- [ ] Database with Firebase
- [ ] CRUD with Firebase
- [ ] Custom Form Validation
- [ ] End-to-end testing 

## Setup

1. Clone this project SSH `git clone git@bitbucket.org:neilbo21/levelup.git`
2. `npm install` ~ you may need `sudo npm install`  
3. `ionic serve` ~ just a simple ionic Tabs template, try `ionic serve --l` to see iOS, Android, Windows
4. Run unit tests `npm test`
5. Run unit tests for CI `npm run test --ci`
6. Run code coverge `npm run test_coverage`, this will generate a coverage folder, open `index.html` file in a browser to see coverage report.